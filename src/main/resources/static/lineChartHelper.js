"use strict";

function drawLineChart(id, actualDatas, numberOfUpdates, maxValueOfYAxis, yText, xText, color, cooperatorColor, cooperatorsAvgPoints) {
	
	const axisDatas = createAxisDatas(numberOfUpdates, maxValueOfYAxis);
	const datas = createDatasObject(actualDatas);
	
	const totalWidth = 330, totalHeight = 220;
	const margin = { top: 30, right: 20, bottom: 40, left: 50 };
	const width = totalWidth - margin.left - margin.right;
	const height = totalHeight - margin.top - margin.bottom;
	
	var x = d3.scaleLinear().rangeRound([0, width]);
	var y = d3.scaleLinear().rangeRound([height, 0]);
	
	var svg = createSvg(id, totalWidth, totalHeight);
	var g = createG(svg, margin);
	var line = createLine(axisDatas, x, y);
	var axisBottom = createAxisBottom(g, x, xText, height, width);
	var axisLeft = createAxisLeft(g, y, yText);
	appendNewPath(g, datas, line, color);
	
	if(cooperatorsAvgPoints !== undefined) {
		const datas2 = createDatasObject(cooperatorsAvgPoints);
		appendNewPath(g, datas2, line, cooperatorColor);
	}
}


function drawMultiLineChart(id, statistics, numberOfUpdates, yText, xText, colors) {
	const axisDatas = createAxisDatas(numberOfUpdates, 100);
	
	const totalWidth = 600, totalHeight = 400;
	const margin = { top: 30, right: 20, bottom: 40, left: 50 };
	const width = totalWidth - margin.left - margin.right;
	const height = totalHeight - margin.top - margin.bottom;

	var x = d3.scaleLinear().rangeRound([0, width]);
	var y = d3.scaleLinear().rangeRound([height, 0]);
	
	var svg = createSvg(id, totalWidth, totalHeight);
	var g = createG(svg, margin);
	var line = createLine(axisDatas, x, y);
	var axisBottom = createAxisBottom(g, x, xText, height, width);
	var axisLeft = createAxisLeft(g, y, yText);
		
	statistics.forEach((stat) => {
		stat.percentOfCooperators[0] = stat.percentOfCooperators[1];
		const datas = createDatasObject(stat.percentOfCooperators);
		appendNewPath(g, datas, line, colors[stat.name]);
	})
}



function appendNewPath(g, datas, line, color) {
	g.append("path")
	.datum(datas)
	.attr("fill", "none")
	.attr("stroke", color)
	.attr("stroke-linejoin", "round")
	.attr("stroke-linecap", "round")
	.attr("stroke-width", 1.5)
	.attr("d", line);
}

function createDatasObject(datasToConvert) {
	const datas = [];
	for(let i = 0; i < datasToConvert.length; i++) {
		const obj = {
			xData: i,
			yData: datasToConvert[i]
		}
		datas.push(obj);
	 }
	return datas;
}

function createAxisLeft(g, y, yText) {
	var axisLeft = g.append("g")
	   .call(d3.axisLeft(y))
	   .append("text")
	   .attr("fill", "#000")
	   .attr("font-size", "1em")
	   .attr("text-anchor", "start")
	   .attr("x", -20)
	   .attr("dx", "1em")
	   .attr("y", -20)
	   .attr("dy", "1em")
	   .text(yText);
	
	return axisLeft;
}

function createAxisBottom(g, x, xText, height, width) {
	var axisBottom = g.append("g")
	   .attr("transform", "translate(0," + height + ")")
	   .call(d3.axisBottom(x))
	   
	axisBottom.append("text")
		.attr("fill", "#000")
	    .attr("y", +20)
	    .attr("dy", "1em")
	    .attr("x", width/2)
	    .attr("dx", "1em")
	    .attr("font-size", "1em")
	    .attr("text-anchor", "middle")
	    .text(xText);
	
	return axisBottom;
}

function createLine(axisDatas, x, y) {
	var line = d3.line()
	   .x(function(axisData) { return x(axisData.xData)})
	   .y(function(axisData) { return y(axisData.yData)})
	   x.domain(d3.extent(axisDatas, function(axisData) { return axisData.xData }));
	   y.domain(d3.extent(axisDatas, function(axisData) { return axisData.yData }));
	   
	return line;
}

function createAxisDatas(numberOfUpdates, maxValueOfYAxis) {
	const axisDatas = []
	
	const maxAxisData = {
		xData: numberOfUpdates,
		yData: maxValueOfYAxis
	}
					
	const minAxisData = {
		xData: 0,
		yData: 0
	}
	
	axisDatas.push(maxAxisData);
	axisDatas.push(minAxisData);
	
	return axisDatas;
}

function createG(svg, margin) {
	var g = svg.append("g")
	.attr("transform", 
		"translate(" + margin.left + "," + margin.top + ")"
	);
	return g;
}

function createSvg(id, totalWidth, totalHeight) {
	var svg = d3.select(id)
		.append("svg")
		.attr("width", totalWidth)
		.attr("height", totalHeight);
	
	return svg;
}