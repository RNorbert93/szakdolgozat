"use strict";

function graphGeneratorChanged(selectedValue) {
	const barabasi = document.getElementsByClassName("barabasi-albert-data");
	const erdos = document.getElementsByClassName("erdos-renyi-data");
	const watts = document.getElementsByClassName("watts-strogatz-data");;
	let forBarabasi = 'inline';
	let forErdos = 'none';
	let forWatts = 'none';
    if(selectedValue === "Erdos-Renyi") {
    	forBarabasi = 'none';
    	forErdos = 'inline';
    	forWatts = 'none';
    } else if(selectedValue === "Watts-Strogatz") {
    	forBarabasi = 'none';
    	forErdos = 'none';
    	forWatts = 'inline';
    }
    for(let i = 0; i < barabasi.length; i++) {
    	barabasi[i].style.display = forBarabasi;
    	erdos[i].style.display = forErdos;
    }
    for(let i = 0; i < watts.length; i++) {
    	watts[i].style.display = forWatts;
    }
}

function updateRuleChanged(selectedValue) {
	const smoothElements = document.getElementsByClassName("smooth-replicator-element");
	let displayValue = 'none';
	if(selectedValue === "SmoothReplicator" || selectedValue === true) {
		displayValue = 'inline';
	}
	for(let i = 0; i < smoothElements.length; i++) {
		smoothElements[i].style.display = displayValue;
    }
}