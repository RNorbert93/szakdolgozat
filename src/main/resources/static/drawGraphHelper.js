"use strict";

function drawGraph(id, graph, coordinates, width, height, r) {
	
	if(graph != null) {
		
		const nodesArray = [];
		const linksArray = [];
		const textsArray = [];
		const vis = d3.select(id)
					.append("svg")
					.attr("width", width)
					.attr("height", height)
		
		graph.nodes.forEach((node) => {
			const newNode = {
				x: coordinates[node.id - 1].x,
				y: coordinates[node.id - 1].y,
				cooperate: node.cooperate,
				id: node.id
			}
			nodesArray.push(newNode);
			const text = {
				source: newNode,
				label: node.id
			}
			textsArray.push(text);
			node.neighbors.forEach((neighborId) => {
				if(nodesArray.find((node) => node.id === neighborId)) {
					const link = {
						source: newNode,
						target: nodesArray.find((node) => node.id === neighborId)
					}
					linksArray.push(link);
				}
			})
		})
		
		
		var edges = createEdges(vis, linksArray);
		var nodes = createNode(vis, nodesArray, dragged);
		var text = createText(vis, textsArray);
			
		function dragged(node) {
			node.x = d3.event.x, node.y = d3.event.y;
			d3.select(this).attr("cx", node.x).attr("cy", node.y);
			edges.filter(function(edge) { return edge.source === node; })
				.attr("x1", node.x).attr("y1", node.y);
			edges.filter(function(edge) { return edge.target === node; })
				.attr("x2", node.x).attr("y2", node.y);
			text.filter(function(text) { return text.source === node; })
				.attr("x", node.x - r/2).attr("y", node.y - r * 1.2);
		  }
	}
}


function createText(vis, textsArray) {
	var text = vis.selectAll("text")
		.data(textsArray)
		.enter()
		.append("text")
		.attr("x", function(text) { return text.source.x - r/2; })
	    .attr("y", function(text) { return text.source.y - r * 1.2; })
	    .text(function(text) { return text.label})
	    .attr("font-size", r)
	    .attr("font-family", "Arial")
	    .attr("fill", "black");
	
	return text;
}

function createNode(vis, nodesArray, dragged) {
	var nodes = vis.selectAll("circle")
		.data(nodesArray)
		.enter()
		.append("circle")
	    .attr("cx", function(node) { return node.x; })
		.attr("cy", function(node) { return node.y; })
		.attr("r", r)
		.attr("fill", function(node) { 
			return node.cooperate ? "#00ff55" : "red"})
		.call(d3.drag().on("drag", dragged));
	
	return nodes;
}

function createEdges(vis, linksArray) {
	var edges = vis.selectAll("line")
	   .data(linksArray)
	   .enter()
	   .append("line")
	   .attr("x1", function(line) { return line.source.x })
	   .attr("y1", function(line) { return line.source.y })
	   .attr("x2", function(line) { return line.target.x })
	   .attr("y2", function(line) { return line.target.y })
	   
	return edges;
}