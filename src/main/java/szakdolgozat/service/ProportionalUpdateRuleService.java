package szakdolgozat.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import org.springframework.stereotype.Service;

import szakdolgozat.model.Graph;
import szakdolgozat.model.Node;

@Service
public class ProportionalUpdateRuleService{
	
	public void update(Graph graph) {
		
		Random random = new Random();
		Node actualNode = graph.getNodeById(random.nextInt(graph.getNodes().size()) + 1);
		List<Integer> neighborsIds = new ArrayList<Integer>();
		
		if(actualNode.getNeighbors().size() > 0) {
			
			int sum = actualNode.getPoints();
			Map<Integer, Integer> pointsByNodes = new HashMap<Integer, Integer>();
			pointsByNodes.put(actualNode.getId(), actualNode.getPoints());
			neighborsIds.addAll(actualNode.getNeighbors());
			
			for(int neighborId : neighborsIds) {
				Node neighborNode = graph.getNodeById(neighborId);
				sum += neighborNode.getPoints();
				pointsByNodes.put(neighborNode.getId(), neighborNode.getPoints());
			}
			
			if(sum != 0) {
				int generatedNumber = random.nextInt(sum) + 1;
				
				for(Entry<Integer, Integer> entry : pointsByNodes.entrySet()) {
					generatedNumber -= entry.getValue();
					if(generatedNumber <= 0) {
						actualNode.setCooperate(graph.getNodeById(entry.getKey()).isCooperate());
						break;
					}
				}
			}else {
				neighborsIds.add(actualNode.getId());
				int selectedNodeToCopyStrategyId = neighborsIds.get(random.nextInt(neighborsIds.size()));
				actualNode.setCooperate(graph.getNodeById(selectedNodeToCopyStrategyId).isCooperate());
			}
		}
	}

}
