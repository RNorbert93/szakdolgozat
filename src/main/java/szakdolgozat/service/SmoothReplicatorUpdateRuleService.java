package szakdolgozat.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Service;

import szakdolgozat.model.Graph;
import szakdolgozat.model.MatrixGame;
import szakdolgozat.model.Node;

@Service
public class SmoothReplicatorUpdateRuleService{
	
	public void update(Graph graph, int numberOfGamesForNodes, int k) {
		
		Random random = new Random();
		Node actualNode = graph.getNodeById(
				random.nextInt(graph.getNodes().size()) + 1);
		List<Integer> neighborsIds = new ArrayList<Integer>();
		
		if(actualNode.getNeighbors().size() > 0){
			
			neighborsIds.addAll(actualNode.getNeighbors());
			Node selectedNeighbor = graph.getNodeById(
					neighborsIds.get(random.nextInt(neighborsIds.size())));
			
			if(actualNode.isCooperate() != selectedNeighbor.isCooperate()) {
				double chanceToCopyStrategy = calculateChance(
						actualNode.getPoints(), selectedNeighbor.getPoints(), k);
				
				if(chanceToCopyStrategy > random.nextDouble()) {
					actualNode.setCooperate(selectedNeighbor.isCooperate());
				}
			}
		}
	}
	
	private double calculateChance(int Gi, int Gj, int k) {
		double forExp = (double) Gj - Gi;
		forExp *= -1;
		if(k != 0) {
			forExp /= k;
		}
		double divider = Math.exp(forExp);
		return ((double) 1 / ((double) 1 + divider));
	}

}
