package szakdolgozat.service;

import java.util.Random;

import org.springframework.stereotype.Service;

import szakdolgozat.model.Graph;
import szakdolgozat.model.Node;

@Service
public class BarabasiAlbertGraphGeneratorService {

	public Graph createGraph(int n, int m, int chanceToStartAsCooperative) {
		
		Random random = new Random();
		Graph graph = new Graph();
		int[] degreesOfNodes = new int[n];
		degreesOfNodes[0]++;
		degreesOfNodes[1]++;
		int sumOfDegrees = 2;
		
		firstStep(random, graph, chanceToStartAsCooperative);
		
		for(int idOfNewNode = 3; idOfNewNode <= n; idOfNewNode++) {
			Node newNode = new Node((random.nextInt(100) + 1) <= chanceToStartAsCooperative ? true : false, idOfNewNode);
			
			if(m >= graph.getNumberOfNodes()) {
				
				for(Node actualNode : graph.getNodes()) {
					newNode.addNeighbor(actualNode);
					actualNode.addNeighbor(newNode);
					degreesOfNodes[actualNode.getId() - 1]++;
					degreesOfNodes[newNode.getId() - 1]++;
					sumOfDegrees += 2;
				}
				
			} else {
				
				int quantityOfNewEdges = m;
				
				while(quantityOfNewEdges > 0) {
					int randomNumberToChooseNode = random.nextInt(sumOfDegrees - degreesOfNodes[newNode.getId() - 1]) + 1;
					int actualIndexOfDegreesOfNodes = 0;
					
					while(randomNumberToChooseNode > 0) {
						randomNumberToChooseNode -= degreesOfNodes[actualIndexOfDegreesOfNodes];
						actualIndexOfDegreesOfNodes++;
					}
					
					Node selectedNode = graph.getNodeById(actualIndexOfDegreesOfNodes);
					
					if(selectedNode != null && !selectedNode.isNeighbors(newNode)) {
						selectedNode.addNeighbor(newNode);
						degreesOfNodes[selectedNode.getId() - 1]++;
						newNode.addNeighbor(selectedNode);
						degreesOfNodes[newNode.getId() - 1]++;
						sumOfDegrees += 2;
						quantityOfNewEdges--;
					}
				}
			}
			
			graph.addNode(newNode);
		}
		
		return graph;
	}
	
	private void firstStep(Random random, Graph graph, int chanceToStartAsCooperative) {
		Node firstNode = new Node((random.nextInt(100) + 1) <= chanceToStartAsCooperative ? true : false, 1);
		Node secondNode = new Node((random.nextInt(100) + 1) <= chanceToStartAsCooperative ? true : false, 2);
		firstNode.addNeighbor(secondNode);
		secondNode.addNeighbor(firstNode);
		graph.addNode(firstNode);
		graph.addNode(secondNode);
	}
}
