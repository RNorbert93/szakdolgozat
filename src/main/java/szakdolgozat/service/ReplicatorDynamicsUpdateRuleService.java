package szakdolgozat.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Service;

import szakdolgozat.model.Graph;
import szakdolgozat.model.MatrixGame;
import szakdolgozat.model.Node;

@Service
public class ReplicatorDynamicsUpdateRuleService{
	
	public void update(Graph graph, int numberOfGamesForNodes) {
		
		Random random = new Random();
		Node actualNode = graph.getNodeById(
				random.nextInt(graph.getNodes().size()) + 1);
		List<Integer> neighborsIds = new ArrayList<Integer>();
		
		if(actualNode.getNeighbors().size() > 0){
			
			neighborsIds.addAll(actualNode.getNeighbors());
			Node selectedNeighbor = graph.getNodeById(
					neighborsIds.get(random.nextInt(neighborsIds.size())));
			
			if(actualNode.isCooperate() != selectedNeighbor.isCooperate() && 
					actualNode.getPoints() < selectedNeighbor.getPoints())
			{
				
				double chance = (((double)selectedNeighbor.getPoints() / numberOfGamesForNodes) -
						((double)actualNode.getPoints() / numberOfGamesForNodes)) /
						10;
				
				if(chance > random.nextDouble()) {
					actualNode.setCooperate(selectedNeighbor.isCooperate());
				}
			}
		}
	}

}
