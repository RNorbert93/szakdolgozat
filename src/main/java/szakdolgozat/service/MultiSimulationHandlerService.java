package szakdolgozat.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import szakdolgozat.model.Graph;
import szakdolgozat.model.MatrixGame;
import szakdolgozat.model.Statistics;

@Service
public class MultiSimulationHandlerService {
	
	@Autowired
	BestTakesOverUpdateRuleService bestTakesOverUpdate;
	
	@Autowired
	ReplicatorDynamicsUpdateRuleService replicationDynamicsUpdate;
	
	@Autowired
	ProportionalUpdateRuleService proportionalUpdate;
	
	@Autowired
	SmoothReplicatorUpdateRuleService smoothReplicatorUpdate;
	
	@Autowired
	PlayGamesForNodesService playGamesForNodesService;
	
	@Autowired
	StatisticsService statService;
	
	@Autowired
	BarabasiAlbertGraphGeneratorService barabasiAlbert;
	
	@Autowired
	ErdosRenyiGraphGeneratorService erdosRenyi;
	
	@Autowired
	WattsStrogatzGraphGeneratorService wattsStrogatz;
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	public void runSimulation(int numberOfUpdates, 
			int numberOfGamesForNodes, MatrixGame game, List<List<Statistics>> statisticsList, 
			int numberOfSimulations, int k, String graphGenerator, List<String> updateRules,
			int numberOfNodes, int chanceToStartAsCoop, int numberOfConnections,
			double edgeProbability, int wattsK, double wattsProbability) {
		
		logger.info("Multi simulation handler service ready to run, number of updates: {}, number of games for nodes: {},"
				+ "number of simulations for each update rule: {}.", numberOfUpdates,
				numberOfGamesForNodes, numberOfSimulations);
		
		for(String updateRuleName : updateRules) {
			List<Statistics> statistics = new ArrayList<Statistics>();
			for(int i = 0; i < numberOfSimulations; i++) {
				Graph graph;
				
				if(graphGenerator.equals("Barabasi-Albert")) {
					graph = barabasiAlbert.createGraph(numberOfNodes,
							numberOfConnections, chanceToStartAsCoop);
				} else if(graphGenerator.equals("Erdos-Renyi")){
					graph = erdosRenyi.createGraph(numberOfNodes, edgeProbability, chanceToStartAsCoop);
				} else {
					graph = wattsStrogatz.createGraph(numberOfNodes, wattsK, 
							wattsProbability, chanceToStartAsCoop);
				}
				
				Statistics statisticsAboutThisSimulation = new Statistics(game.getMaxPayoff());
				statisticsAboutThisSimulation.setName(updateRuleName);
				statService.calculateStatisticsAfterUpdate(graph, statisticsAboutThisSimulation, numberOfGamesForNodes, true);
				
				for( int actualRound = 1; actualRound <= numberOfUpdates; actualRound++) {
					playGamesForNodesService.playGames(graph, numberOfGamesForNodes, game);
					
					if(updateRuleName.equals("best-takes-over")) {
						bestTakesOverUpdate.update(graph);
					} else if(updateRuleName.equals("proportional")) {
						proportionalUpdate.update(graph);
					} else if(updateRuleName.equals("replicator-dynamics")) {
						replicationDynamicsUpdate.update(graph, numberOfGamesForNodes);
					} else if(updateRuleName.equals("smooth-replicator")) {
						smoothReplicatorUpdate.update(graph, numberOfGamesForNodes, k);
					}
					
					statService.calculateStatisticsAfterUpdate(graph, statisticsAboutThisSimulation, numberOfGamesForNodes, true);
					graph.clearPoints();
				}
				statistics.add(statisticsAboutThisSimulation);
			}
			statisticsList.add(statistics);
		}
	}
}
