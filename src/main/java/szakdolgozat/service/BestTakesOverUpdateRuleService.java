package szakdolgozat.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Service;

import szakdolgozat.model.Graph;
import szakdolgozat.model.Node;

@Service
public class BestTakesOverUpdateRuleService{
	
	public void update(Graph graph) {
		
		Random random = new Random();
		Node actualNode = graph.getNodeById(random.nextInt(graph.getNodes().size()) + 1);
		List<Integer> neighborsIds = new ArrayList<Integer>();
		
		if(actualNode.getNeighbors().size() > 0) {
			
			neighborsIds.addAll(actualNode.getNeighbors());
			int maxPoints = actualNode.getPoints();
			boolean strategy = actualNode.isCooperate();
			
			for(Integer neighbor : neighborsIds) {
				if(maxPoints < graph.getNodeById(neighbor).getPoints()) {
					maxPoints = graph.getNodeById(neighbor).getPoints();
					strategy = graph.getNodeById(neighbor).isCooperate();
				}
			}
			
			actualNode.setCooperate(strategy);
		}
	}
}
