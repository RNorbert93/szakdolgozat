package szakdolgozat.service;

import java.util.Random;

import org.springframework.stereotype.Service;

import szakdolgozat.model.Graph;
import szakdolgozat.model.Node;

@Service
public class ErdosRenyiGraphGeneratorService{

	public Graph createGraph(int n, double p, int chanceToStartAsCooperative) {
		
		Random random = new Random();
		Graph graph = new Graph();
		
		for(int idOfNewNode = 1; idOfNewNode <= n; idOfNewNode++) {
			Node newNode = new Node((random.nextInt(100) + 1) <= chanceToStartAsCooperative ? true : false, idOfNewNode);
			
			for(Node actualNode : graph.getNodes()) {
				if(random.nextDouble() <= p) {
					newNode.addNeighbor(actualNode);
					actualNode.addNeighbor(newNode);
				}
			}
			
			graph.addNode(newNode);
		}
		
		return graph;
	}
}
