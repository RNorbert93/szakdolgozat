package szakdolgozat.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import szakdolgozat.model.Graph;
import szakdolgozat.model.MatrixGame;
import szakdolgozat.model.Statistics;

@Service
public class SimulationHandlerService {
	
	@Autowired
	BestTakesOverUpdateRuleService bestTakesOverUpdate;
	
	@Autowired
	ReplicatorDynamicsUpdateRuleService replicationDynamicsUpdate;
	
	@Autowired
	ProportionalUpdateRuleService proportionalUpdate;
	
	@Autowired
	SmoothReplicatorUpdateRuleService smoothReplicatorUpdate;
	
	@Autowired
	PlayGamesForNodesService playGamesForNodesService;
	
	@Autowired
	StatisticsService statService;
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	public List<Graph> runSimulation(Graph graph, String updateRule, int numberOfUpdates, 
			int numberOfGamesForNodes, MatrixGame game, Statistics statistics,
			int drawGraphAfterThisNumberOfUpdates, List<Integer> drawGraphAtTheseUpdates,
			int k) {
		
		logger.info("Simulation handler service ready to run, number of updates: {}, number of games for nodes: {}.",
				numberOfUpdates, numberOfGamesForNodes);
		
		List<Graph> graphs = new ArrayList<Graph>();
		
		graphs.add(Graph.newInstance(graph));
		drawGraphAtTheseUpdates.add(0);
		boolean lastGraphAdded = false;
		
		statService.calculateStatisticsAfterUpdate(graph, statistics, numberOfGamesForNodes, false);
		for( int actualRound = 1; actualRound <= numberOfUpdates; actualRound++) {
			playGamesForNodesService.playGames(graph, numberOfGamesForNodes, game);
			
			if(updateRule.equals("BestTakesOver")) {
				bestTakesOverUpdate.update(graph);
			} else if(updateRule.equals("Proportional")) {
				proportionalUpdate.update(graph);
			} else if(updateRule.equals("ReplicatorDynamics")) {
				replicationDynamicsUpdate.update(graph, numberOfGamesForNodes);
			} else if(updateRule.equals("SmoothReplicator")) {
				smoothReplicatorUpdate.update(graph, numberOfGamesForNodes, k);
			}
			statService.calculateStatisticsAfterUpdate(graph, statistics, numberOfGamesForNodes, false);
			
			if(!lastGraphAdded) {
				if(!isGraphHaveTwoStrategyYet(graph)) {
					lastGraphAdded = true;
					drawGraphAtTheseUpdates.add(actualRound);
					graphs.add(Graph.newInstance(graph));
				} else {
					if(actualRound % drawGraphAfterThisNumberOfUpdates == 0) {
						drawGraphAtTheseUpdates.add(actualRound);
						graphs.add(Graph.newInstance(graph));
					}
				}
			}
			graph.clearPoints();
		}
		
		if(!lastGraphAdded) {
			if(numberOfUpdates % drawGraphAfterThisNumberOfUpdates != 0) {
				drawGraphAtTheseUpdates.add(numberOfUpdates);
				graphs.add(Graph.newInstance(graph));
			}
		}
		
		return graphs;
	}
	
	private boolean isGraphHaveTwoStrategyYet(Graph graph) {
		int numberOfCooperators = (int) graph.getNodes().stream()
				.filter(node -> node.isCooperate() == true).count();
		
		if(numberOfCooperators == 0 || numberOfCooperators == graph.getNumberOfNodes()) return false;
		return true;
	}
}
