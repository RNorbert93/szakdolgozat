package szakdolgozat.service;

import java.util.Random;

import org.springframework.stereotype.Service;

import szakdolgozat.model.Graph;
import szakdolgozat.model.Node;

@Service
public class WattsStrogatzGraphGeneratorService {

	public Graph createGraph(int numberOfNodes, int k, double p, int chanceToStartAsCooperative) {
		
		Random random = new Random();
		Graph graph = new Graph();
		
		createRing(graph, numberOfNodes, k, chanceToStartAsCooperative, random);
		replaceEdges(graph, numberOfNodes, k, p, random);
		
		return graph;
	}
	
	private void createRing(Graph graph, int numberOfNodes, int k, int chanceToStartAsCooperative, Random random) {
		
		for(int i = 1; i <= numberOfNodes; i++) {
			graph.addNode(new Node((random.nextInt(100) + 1) <= chanceToStartAsCooperative ? true : false, i));
		}
		
		for(int i = 1; i <= numberOfNodes; i++) {
			Node node = graph.getNodeById(i);
			
			for(int j = 1; j <= k / 2; j++) {
				Node neighbor = graph.getNodeById(getNodeIdToSearch(i + j, numberOfNodes));
				node.addNeighbor(neighbor);
				neighbor.addNeighbor(node);
			}
		}
	}
	
	private void replaceEdges(Graph graph, int numberOfNodes, int k, double p, Random random) {
		for(int i = 1; i <= numberOfNodes; i++) {
			for(int j = 1; j <= k / 2; j++) {
				if(random.nextDouble() < p) {
					
					Node node = graph.getNodeById(i);
					Node neighborToDelete = graph.getNodeById(getNodeIdToSearch(i + j, numberOfNodes));
					node.deleteNeighbor(neighborToDelete);
					neighborToDelete.deleteNeighbor(node);
					
					Node newNeighbor = graph.getNodeById(random.nextInt(numberOfNodes) + 1);;
					while(newNeighbor.getId() == node.getId() || node.isNeighbors(newNeighbor)) {
						newNeighbor = graph.getNodeById(random.nextInt(numberOfNodes) + 1);
					}
					node.addNeighbor(newNeighbor);
					newNeighbor.addNeighbor(node);
				}
			}
		}
	}
	
	private int getNodeIdToSearch(int id, int numberOfNodes) {
		while(id > numberOfNodes) {
			id -= numberOfNodes;
		} return id;
	}
}
