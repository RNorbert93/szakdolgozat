package szakdolgozat.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import szakdolgozat.model.Graph;
import szakdolgozat.model.Node;
import szakdolgozat.model.Statistics;

@Service
public class StatisticsService {

	public void calculateStatisticsAfterUpdate(Graph graph, Statistics statistics, int numberOfGamesForNodes, boolean needOnlyThePercent) {
		statistics.addToPercentOfCooperatesList(calculatePercentOfCooperates(graph));
		if(!needOnlyThePercent) {
			statistics.addToAvgPointsOfAllNodes(calculateAvgPointsOfAllNodes(graph, numberOfGamesForNodes));
			statistics.addToCooperatorsAvgPoints(calculateAvgPointsByStrategy(graph, numberOfGamesForNodes, true));
			statistics.addToDefectorsAvgPoints(calculateAvgPointsByStrategy(graph, numberOfGamesForNodes, false));
		}
	}
	
	private int calculatePercentOfCooperates(Graph graph) {
		int numberOfCooperates = (int) graph.getNodes().stream()
				.filter(node -> node.isCooperate() == true).count();
		return (int)(((double)numberOfCooperates / graph.getNumberOfNodes()) * 100);
	}
	
	private double calculateAvgPointsOfAllNodes(Graph graph, int numberOfGamesForNodes) {
		double sumOfAvgs = 0;
		for(Node actualNode : graph.getNodes()) {
			sumOfAvgs += ((double)actualNode.getPoints()) / numberOfGamesForNodes;
		}
		return (sumOfAvgs / graph.getNodes().size());
	}
	
	private double calculateAvgPointsByStrategy(Graph graph, int numberOfGamesForNodes, boolean cooperator) {
		Node[] nodes = graph.getNodes().stream()
				.filter(node -> node.isCooperate() == cooperator)
				.toArray(Node[]::new);
		
		if(nodes.length == 0) return 0;
		double sumOfAvgs = 0;
		for(Node actualNode : nodes) {
			sumOfAvgs += ((double)actualNode.getPoints()) / numberOfGamesForNodes;
		}
		sumOfAvgs /= nodes.length;
		return sumOfAvgs;
	}
	
	public List<Statistics> calculateAvgStatistics(List<List<Statistics>> statisticsList) {
		List<Statistics> actualStatList = new ArrayList<Statistics>();
		for(List<Statistics> statList : statisticsList) {
			Statistics actualStat = new Statistics(statList.get(0).getMaxPayoff());
			actualStat.setName(statList.get(0).getName());
			int[] avgValues = new int[statList.get(0).getPercentOfCooperators().size()];
			for(Statistics stat : statList) {
				List<Integer> percentOfCooperators = stat.getPercentOfCooperators();
				for(int i = 0; i < percentOfCooperators.size(); i++) {
					avgValues[i] += percentOfCooperators.get(i);
				}
			}
			int size = statList.size();
			for(int i = 0; i < avgValues.length; i++) {
				actualStat.addToPercentOfCooperatesList(avgValues[i] /= size);
			}
			actualStatList.add(actualStat);
		}
		return actualStatList;
	}
}
