package szakdolgozat.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import szakdolgozat.model.Graph;
import szakdolgozat.model.MatrixGame;
import szakdolgozat.model.Node;

import org.springframework.stereotype.Service;

@Service
public class PlayGamesForNodesService {

	public void playGames(Graph graph, int numberOfGamesForNodes, MatrixGame game) {
		Random random = new Random();
		for(Node actualNode : graph.getNodes()) {
			List<Integer> neighborsIds = new ArrayList<Integer>();
			neighborsIds.addAll(actualNode.getNeighbors());
			if(neighborsIds.size() > 0) {
				for(int i = 0; i < numberOfGamesForNodes; i++) {
					actualNode.addPoints(game.getGamePointsForOneNode(
							actualNode, graph.getNodeById(neighborsIds.get(random.nextInt(neighborsIds.size())))));
				}
			}
		}
	}
}
