package szakdolgozat.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("set-multi-simulation-datas.html")
public class SetMultiSimulationDatasController {
	
	@RequestMapping(method = RequestMethod.GET)
	public String getThePage() {
		return "set-multi-simulation-datas.html";
	}

}