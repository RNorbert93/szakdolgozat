package szakdolgozat.controller.form;

import javax.validation.constraints.NotBlank;

public class SetSimulationDatasForm {

	@NotBlank
	private int bothCooperators;
	@NotBlank
	private int bothDefectors;
	@NotBlank
	private int defectorPrize;
	@NotBlank
	private int cooperatorPenalty;
	@NotBlank
	private int numberOfNodes;
	@NotBlank
	private double edgeProbability;
	@NotBlank
	private int nodeChanceToStartAsCooperative;
	@NotBlank
	private int numberOfConnections;
	@NotBlank
	private int numberOfGamesForNodes;
	@NotBlank
	private int numberOfUpdates;
	@NotBlank
	private int drawGraphAfterThisNumberOfUpdates;
	@NotBlank
	private String graphGenerator;
	@NotBlank
	private String updateRule;
	@NotBlank
	private int k;
	@NotBlank
	private int wattsStrogatzK;
	@NotBlank
	private double wattsStrogatzProbability;
	
	public int getBothCooperators() {
		return bothCooperators;
	}
	public void setBothCooperators(int boothCooperators) {
		this.bothCooperators = boothCooperators;
	}
	public int getBothDefectors() {
		return bothDefectors;
	}
	public void setBothDefectors(int bothDefectors) {
		this.bothDefectors = bothDefectors;
	}
	public int getDefectorPrize() {
		return defectorPrize;
	}
	public void setDefectorPrize(int defectorPrize) {
		this.defectorPrize = defectorPrize;
	}
	public int getCooperatorPenalty() {
		return cooperatorPenalty;
	}
	public void setCooperatorPenalty(int cooperatorPenalty) {
		this.cooperatorPenalty = cooperatorPenalty;
	}
	public int getNumberOfNodes() {
		return numberOfNodes;
	}
	public void setNumberOfNodes(int numberOfNodes) {
		this.numberOfNodes = numberOfNodes;
	}
	public int getNodeChanceToStartAsCooperative() {
		return nodeChanceToStartAsCooperative;
	}
	public void setNodeChanceToStartAsCooperative(int nodeChanceToStartAsCooperative) {
		this.nodeChanceToStartAsCooperative = nodeChanceToStartAsCooperative;
	}
	public double getEdgeProbability() {
		return edgeProbability;
	}
	public void setEdgeProbability(double edgeProbability) {
		this.edgeProbability = edgeProbability;
	}
	public int getNumberOfConnections() {
		return numberOfConnections;
	}
	public void setNumberOfConnections(int numberOfConnections) {
		this.numberOfConnections = numberOfConnections;
	}
	public int getNumberOfGamesForNodes() {
		return numberOfGamesForNodes;
	}
	public void setNumberOfGamesForNodes(int numberOfGamesForNodes) {
		this.numberOfGamesForNodes = numberOfGamesForNodes;
	}
	public int getNumberOfUpdates() {
		return numberOfUpdates;
	}
	public void setNumberOfUpdates(int numberOfUpdates) {
		this.numberOfUpdates = numberOfUpdates;
	}
	public int getDrawGraphAfterThisNumberOfUpdates() {
		return drawGraphAfterThisNumberOfUpdates;
	}
	public void setDrawGraphAfterThisNumberOfUpdates(int drawGraphAfterThisNumberOfUpdates) {
		this.drawGraphAfterThisNumberOfUpdates = drawGraphAfterThisNumberOfUpdates;
	}
	public String getGraphGenerator() {
		return graphGenerator;
	}
	public void setGraphGenerator(String graphGenerator) {
		this.graphGenerator = graphGenerator;
	}
	public String getUpdateRule() {
		return updateRule;
	}
	public void setUpdateRule(String updateRule) {
		this.updateRule = updateRule;
	}
	public int getK() {
		return k;
	}
	public void setK(int k) {
		this.k = k;
	}
	public double getWattsStrogatzProbability() {
		return wattsStrogatzProbability;
	}
	public void setWattsStrogatzProbability(double wattsStrogatzProbability) {
		this.wattsStrogatzProbability = wattsStrogatzProbability;
	}
	public int getWattsStrogatzK() {
		return wattsStrogatzK;
	}
	public void setWattsStrogatzK(int wattsStrogatzK) {
		this.wattsStrogatzK = wattsStrogatzK;
	}
	
	
}
