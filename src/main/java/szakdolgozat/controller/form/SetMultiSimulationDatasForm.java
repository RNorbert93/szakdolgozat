package szakdolgozat.controller.form;

import javax.validation.constraints.NotBlank;

public class SetMultiSimulationDatasForm {

	@NotBlank
	private int bothCooperators;
	@NotBlank
	private int bothDefectors;
	@NotBlank
	private int defectorPrize;
	@NotBlank
	private int cooperatorPenalty;
	@NotBlank
	private int numberOfNodes;
	@NotBlank
	private double edgeProbability;
	@NotBlank
	private int nodeChanceToStartAsCooperative;
	@NotBlank
	private int numberOfConnections;
	@NotBlank
	private int numberOfGamesForNodes;
	@NotBlank
	private int numberOfUpdates;
	@NotBlank
	private String graphGenerator;
	@NotBlank
	private int k;
	@NotBlank
	private boolean bestTakesOver;
	@NotBlank
	private boolean proportional;
	@NotBlank
	private boolean replicatorDynamics;
	@NotBlank
	private boolean smoothReplicator;
	@NotBlank
	private int numberOfSimulations;
	@NotBlank
	private String bestTakesOverColor;
	@NotBlank
	private String proportionalColor;
	@NotBlank
	private String replicatorDynamicsColor;
	@NotBlank
	private String smoothReplicatorColor;
	@NotBlank
	private int wattsStrogatzK;
	@NotBlank
	private double wattsStrogatzProbability;
	
	public int getBothCooperators() {
		return bothCooperators;
	}
	public void setBothCooperators(int boothCooperators) {
		this.bothCooperators = boothCooperators;
	}
	public int getBothDefectors() {
		return bothDefectors;
	}
	public void setBothDefectors(int bothDefectors) {
		this.bothDefectors = bothDefectors;
	}
	public int getDefectorPrize() {
		return defectorPrize;
	}
	public void setDefectorPrize(int defectorPrize) {
		this.defectorPrize = defectorPrize;
	}
	public int getCooperatorPenalty() {
		return cooperatorPenalty;
	}
	public void setCooperatorPenalty(int cooperatorPenalty) {
		this.cooperatorPenalty = cooperatorPenalty;
	}
	public int getNumberOfNodes() {
		return numberOfNodes;
	}
	public void setNumberOfNodes(int numberOfNodes) {
		this.numberOfNodes = numberOfNodes;
	}
	public int getNodeChanceToStartAsCooperative() {
		return nodeChanceToStartAsCooperative;
	}
	public void setNodeChanceToStartAsCooperative(int nodeChanceToStartAsCooperative) {
		this.nodeChanceToStartAsCooperative = nodeChanceToStartAsCooperative;
	}
	public double getEdgeProbability() {
		return edgeProbability;
	}
	public void setEdgeProbability(double edgeProbability) {
		this.edgeProbability = edgeProbability;
	}
	public int getNumberOfConnections() {
		return numberOfConnections;
	}
	public void setNumberOfConnections(int numberOfConnections) {
		this.numberOfConnections = numberOfConnections;
	}
	public int getNumberOfGamesForNodes() {
		return numberOfGamesForNodes;
	}
	public void setNumberOfGamesForNodes(int numberOfGamesForNodes) {
		this.numberOfGamesForNodes = numberOfGamesForNodes;
	}
	public int getNumberOfUpdates() {
		return numberOfUpdates;
	}
	public void setNumberOfUpdates(int numberOfUpdates) {
		this.numberOfUpdates = numberOfUpdates;
	}
	public String getGraphGenerator() {
		return graphGenerator;
	}
	public void setGraphGenerator(String graphGenerator) {
		this.graphGenerator = graphGenerator;
	}
	public int getK() {
		return k;
	}
	public void setK(int k) {
		this.k = k;
	}
	public boolean isBestTakesOver() {
		return bestTakesOver;
	}
	public void setBestTakesOver(boolean bestTakesOver) {
		this.bestTakesOver = bestTakesOver;
	}
	public boolean isProportional() {
		return proportional;
	}
	public void setProportional(boolean proportional) {
		this.proportional = proportional;
	}
	public boolean isReplicatorDynamics() {
		return replicatorDynamics;
	}
	public void setReplicatorDynamics(boolean replicatorDynamics) {
		this.replicatorDynamics = replicatorDynamics;
	}
	public boolean isSmoothReplicator() {
		return smoothReplicator;
	}
	public void setSmoothReplicator(boolean smoothReplicator) {
		this.smoothReplicator = smoothReplicator;
	}
	public int getNumberOfSimulations() {
		return numberOfSimulations;
	}
	public void setNumberOfSimulations(int numberOfSimulations) {
		this.numberOfSimulations = numberOfSimulations;
	}
	public String getBestTakesOverColor() {
		return bestTakesOverColor;
	}
	public void setBestTakesOverColor(String bestTakesOverColor) {
		this.bestTakesOverColor = bestTakesOverColor;
	}
	public String getProportionalColor() {
		return proportionalColor;
	}
	public void setProportionalColor(String proportionalColor) {
		this.proportionalColor = proportionalColor;
	}
	public String getReplicatorDynamicsColor() {
		return replicatorDynamicsColor;
	}
	public void setReplicatorDynamicsColor(String replicatorDynamicsColor) {
		this.replicatorDynamicsColor = replicatorDynamicsColor;
	}
	public String getSmoothReplicatorColor() {
		return smoothReplicatorColor;
	}
	public void setSmoothReplicatorColor(String smoothReplicatorColor) {
		this.smoothReplicatorColor = smoothReplicatorColor;
	}
	public int getWattsStrogatzK() {
		return wattsStrogatzK;
	}
	public void setWattsStrogatzK(int wattsStrogatzK) {
		this.wattsStrogatzK = wattsStrogatzK;
	}
	public double getWattsStrogatzProbability() {
		return wattsStrogatzProbability;
	}
	public void setWattsStrogatzProbability(double wattsStrogatzProbability) {
		this.wattsStrogatzProbability = wattsStrogatzProbability;
	}
	
}
