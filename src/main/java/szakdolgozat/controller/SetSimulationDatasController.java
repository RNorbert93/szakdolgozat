package szakdolgozat.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("set-simulation-datas.html")
public class SetSimulationDatasController {
	
	@RequestMapping(method = RequestMethod.GET)
	public String getThePage() {
		return "set-simulation-datas.html";
	}

}
