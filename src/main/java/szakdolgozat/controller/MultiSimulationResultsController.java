package szakdolgozat.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import szakdolgozat.controller.form.SetMultiSimulationDatasForm;
import szakdolgozat.model.MatrixGame;
import szakdolgozat.model.Statistics;
import szakdolgozat.service.MultiSimulationHandlerService;
import szakdolgozat.service.StatisticsService;

@Controller
@RequestMapping("multi-simulation-results.html")
public class MultiSimulationResultsController {
	
	@Autowired
	MultiSimulationHandlerService simulation;
	
	@Autowired
	StatisticsService statService;
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	@RequestMapping(method = RequestMethod.GET)
	public String getThePage() {
		return "multi-simulation-results.html";
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String getSimulateResults(Model model, SetMultiSimulationDatasForm form) {
		
		logger.info("A simulation will start soon, graph generator: {}, update rules: {},{},{},{}",
				form.getGraphGenerator(), form.isBestTakesOver(), form.isProportional(),
				form.isReplicatorDynamics(), form.isSmoothReplicator());
		
		int numberOfUpdates = form.getNumberOfUpdates();
		List<String> updateRules = new ArrayList<String>();
		Map<String, String> colors = new HashMap<String,String>();
		
		if(form.isBestTakesOver()) {
			updateRules.add("best-takes-over");
			colors.put("best-takes-over", form.getBestTakesOverColor());
		}
		if(form.isProportional()) {
			updateRules.add("proportional");
			colors.put("proportional", form.getProportionalColor());
		}
		if(form.isReplicatorDynamics()) {
			updateRules.add("replicator-dynamics");
			colors.put("replicator-dynamics", form.getReplicatorDynamicsColor());
		}
		if(form.isSmoothReplicator()) {
			updateRules.add("smooth-replicator");
			colors.put("smooth-replicator", form.getSmoothReplicatorColor());
		}
		
		MatrixGame game = new MatrixGame(form.getBothCooperators(), 
				form.getBothDefectors(), form.getDefectorPrize(), form.getCooperatorPenalty());
		
		List<List<Statistics>> statisticsList = new ArrayList<List<Statistics>>();
		
		simulation.runSimulation(numberOfUpdates, form.getNumberOfGamesForNodes(), game,
				statisticsList, form.getNumberOfSimulations(), form.getK(), form.getGraphGenerator(),
				updateRules, form.getNumberOfNodes(), form.getNodeChanceToStartAsCooperative(),
				form.getNumberOfConnections(), form.getEdgeProbability(),
				form.getWattsStrogatzK(), form.getWattsStrogatzProbability());
		
		List<Statistics> avgStats = statService.calculateAvgStatistics(statisticsList);
		
		logger.info("Simulation is finished!");

		model.addAttribute("statisticsList", statisticsList);
		model.addAttribute("numberOfUpdates", numberOfUpdates);
		model.addAttribute("avgStats", avgStats);
		model.addAttribute("colors", colors);
		return "multi-simulation-results.html";
	}
}