package szakdolgozat.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import szakdolgozat.controller.form.SetSimulationDatasForm;
import szakdolgozat.model.Graph;
import szakdolgozat.model.MatrixGame;
import szakdolgozat.model.Statistics;
import szakdolgozat.service.BarabasiAlbertGraphGeneratorService;
import szakdolgozat.service.ErdosRenyiGraphGeneratorService;
import szakdolgozat.service.SimulationHandlerService;
import szakdolgozat.service.WattsStrogatzGraphGeneratorService;

@Controller
@RequestMapping("simulation-results.html")
public class SimulationResultsController {
	
	@Autowired
	SimulationHandlerService simulation;
	
	@Autowired
	ErdosRenyiGraphGeneratorService erdosRenyi;
	
	@Autowired
	BarabasiAlbertGraphGeneratorService barabasiAlbert;
	
	@Autowired
	WattsStrogatzGraphGeneratorService wattsStrogatz;
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	@RequestMapping(method = RequestMethod.GET)
	public String getThePage() {
		return "simulation-results.html";
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String getSimulateResults(Model model, SetSimulationDatasForm form) {
		
		int numberOfUpdates = form.getNumberOfUpdates();
		int drawGraphAfterThisNumberOfUpdates = form.getDrawGraphAfterThisNumberOfUpdates();
		List<Integer> drawGraphAtTheseUpdates = new ArrayList<Integer>();
		
		logger.info("A simulation will start soon, graph generator: {}, update rule: {}", form.getGraphGenerator(), form.getUpdateRule());
		logger.info("Simulation datas: number of nodes: {}, prizes: {} {} {} {}", form.getNumberOfNodes(),
				form.getBothCooperators(), form.getCooperatorPenalty(), form.getDefectorPrize(), form.getBothDefectors());
		
		MatrixGame game = new MatrixGame(form.getBothCooperators(), 
				form.getBothDefectors(), form.getDefectorPrize(), form.getCooperatorPenalty());
		Graph graph;
		
		if(form.getGraphGenerator().equals("Barabasi-Albert")) {
			graph = barabasiAlbert.createGraph(form.getNumberOfNodes(),
					form.getNumberOfConnections(), form.getNodeChanceToStartAsCooperative());
		} else if(form.getGraphGenerator().equals("Erdos-Renyi")){
			graph = erdosRenyi.createGraph(form.getNumberOfNodes(), form.getEdgeProbability(), form.getNodeChanceToStartAsCooperative());
		} else {
			graph = wattsStrogatz.createGraph(form.getNumberOfNodes(), form.getWattsStrogatzK(), 
					form.getWattsStrogatzProbability(), form.getNodeChanceToStartAsCooperative());
		}
		
		Statistics statistics= new Statistics(game.getMaxPayoff());
		
		List<Graph> graphs;
		
		graphs = simulation.runSimulation(graph, form.getUpdateRule(), numberOfUpdates,
				form.getNumberOfGamesForNodes(), game, statistics, drawGraphAfterThisNumberOfUpdates, 
				drawGraphAtTheseUpdates, form.getK());

		model.addAttribute("graphs", graphs);
		model.addAttribute("statistics", statistics);
		model.addAttribute("numberOfUpdates", numberOfUpdates);
		model.addAttribute("drawGraphAtTheseUpdates", drawGraphAtTheseUpdates);
		
		logger.info("Simulation is over, last update: {}", drawGraphAtTheseUpdates.get(drawGraphAtTheseUpdates.size() - 1));
		
		return "simulation-results.html";
	}
}
