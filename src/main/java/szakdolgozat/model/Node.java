package szakdolgozat.model;

import java.util.HashSet;
import java.util.Set;

public class Node {

	private Set<Integer> neighbors;
	private boolean isCooperate;
	private int id;
	private int points;
	
	public Node(boolean isCooperate, int id) {
		this.isCooperate = isCooperate;
		this.id = id;
		neighbors = new HashSet<Integer>();
		points = 0;
	}
	
	public Node(boolean isCooperate, int id, int points, Set<Integer> neighbors) {
		this.isCooperate = isCooperate;
		this.id = id;
		this.points = points;
		this.neighbors = neighbors;
	}
	
	public static Node newInstance(Node node) {
		return new Node(node.isCooperate(), node.getId(), node.getPoints(), node.getNeighbors());
	}

	public boolean isCooperate() {
		return isCooperate;
	}

	public void setCooperate(boolean isCooperate) {
		this.isCooperate = isCooperate;
	}
	
	public void addNeighbor(Node newNeighbor) {
		this.neighbors.add(newNeighbor.getId());
	}

	public int getId() {
		return id;
	}
	
	public Set<Integer> getNeighbors() {
		return neighbors;
	}
	
	public boolean isNeighbors(Node node) {
		return neighbors.contains(node.getId());
	}
	
	public void deleteNeighbor(Node neighbor) {
		if(neighbors.contains(neighbor.getId())) {
			neighbors.remove(neighbor.getId());
		}
	}
	
	public void addPoints(int points) {
		this.points += points;
	}
	
	public int getPoints() {
		return this.points;
	}
	
	public void clearPoints() {
		this.points = 0;
	}
	
}
