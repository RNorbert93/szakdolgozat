package szakdolgozat.model;

public class MatrixGame {

	private int boothCooperators;
	private int bothDefectors;
	private int defectorPrize;
	private int cooperatorPenalty;
	
	public MatrixGame(int boothCooperators, int bothDefectors, int defectorPrize, int cooperatorPenalty) {
		this.boothCooperators = boothCooperators;
		this.bothDefectors = bothDefectors;
		this.defectorPrize = defectorPrize;
		this.cooperatorPenalty = cooperatorPenalty;
	}

	public int getGamePointsForOneNode(Node node, Node opponent) {
		if(node.isCooperate()) {
			return opponent.isCooperate() ? boothCooperators : cooperatorPenalty;
		} else {
			return opponent.isCooperate() ? defectorPrize : bothDefectors;
		}
	}
	
	public int getMaxPayoff() {
		if(defectorPrize >= boothCooperators && defectorPrize >= bothDefectors && defectorPrize >= cooperatorPenalty) {
			return defectorPrize;
		} else if(boothCooperators >= bothDefectors && boothCooperators >= cooperatorPenalty) {
			return boothCooperators;
		} else if(bothDefectors >= cooperatorPenalty) {
			return bothDefectors;
		} else return cooperatorPenalty;
	}
	
}
