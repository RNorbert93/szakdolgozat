package szakdolgozat.model;

import java.util.ArrayList;
import java.util.List;

public class Graph {
	
	private List<Node> nodes;
	
	public Graph() {
		this.nodes = new ArrayList<Node>();
	}
	
	public Graph(List<Node> nodes) {
		this.nodes = nodes;
	}
	
	public static Graph newInstance(Graph graph) {
		Graph newGraphInstance = new Graph();
		for(Node node : graph.getNodes()) {
			newGraphInstance.addNode(Node.newInstance(node));
		}
		return newGraphInstance;
	}
	
	public void addNode(Node node) {
		nodes.add(node);
	}

	public List<Node> getNodes() {
		return nodes;
	}
	
	public int getNumberOfNodes() {
		return nodes.size();
	}
	
	public Node getNodeById(int id) {
		if(nodes.get(id - 1).getId() == id) {
			return nodes.get(id - 1);
		}
		for(Node node : nodes) {
			if(node.getId() == id) {
				return node;
			}
		}
		return null;
	}
	
	public void clearPoints() {
		for(Node node : nodes) {
			node.clearPoints();
		}
	}
	
}
