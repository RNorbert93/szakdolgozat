package szakdolgozat.model;

import java.util.ArrayList;
import java.util.List;

public class Statistics {

	private List<Integer> percentOfCooperators;
	private List<Double> avgPointsOfAllNodes;
	private List<Double> cooperatorsAvgPoints;
	private List<Double> defectorsAvgPoints;
	private int maxPayoff;
	private String name;
	
	public Statistics(int maxPayoff) {
		this.percentOfCooperators = new ArrayList<Integer>();
		this.avgPointsOfAllNodes = new ArrayList<Double>();
		this.cooperatorsAvgPoints = new ArrayList<Double>();
		this.defectorsAvgPoints = new ArrayList<Double>();
		this.maxPayoff = maxPayoff;
	}

	public List<Integer> getPercentOfCooperators() {
		return percentOfCooperators;
	}
	
	public void addToPercentOfCooperatesList(int percent) {
		percentOfCooperators.add(percent);
	}
	
	public List<Double> getAvgPointsOfAllNodes() {
		return avgPointsOfAllNodes;
	}
	
	public void addToAvgPointsOfAllNodes(double avgPoints) {
		avgPointsOfAllNodes.add(avgPoints);
	}
	
	public List<Double> getCooperatorsAvgPoints() {
		return cooperatorsAvgPoints;
	}
	
	public List<Double> getDefectorsAvgPoints() {
		return defectorsAvgPoints;
	}
	
	public void addToCooperatorsAvgPoints(double avgPoints) {
		cooperatorsAvgPoints.add(avgPoints);
	}
	
	public void addToDefectorsAvgPoints(double avgPoints) {
		defectorsAvgPoints.add(avgPoints);
	}

	public int getMaxPayoff() {
		return maxPayoff;
	}

	public void setMaxPayoff(int maxPayoff) {
		this.maxPayoff = maxPayoff;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
